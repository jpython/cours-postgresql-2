\COPY cours(code,titre,duree) FROM 'cours.csv' DELIMITER ',' CSV;
\COPY instructeurs(nom,prenom) FROM 'instructeurs.csv' DELIMITER ',' CSV;
\COPY enseigne(id_instructeur,id_cours) FROM 'enseigne.csv' DELIMITER ',' CSV;
\COPY sessions(id_cours,date,nb_inscrits,id_instructeur) FROM 'sessions.csv' DELIMITER ',' CSV;
