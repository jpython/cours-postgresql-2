# Cours PosgreSQL 2

## Atelier 1

Installation de PostgreSQL et création d'une première base

- Je vous suggère d'utiliser une distribution de GNU/Linux de
  type Debian (i.e. Debian ou Ubuntu, Mint, etc.)
- Vous pouvez néanmoins choisir un autre système avec lequel vous
  êtes plus à l'aise, autre distribution de GNU/Linux, autre UNIX
  (Mac OS) ou MS Windows... Cependant je ne pourrais fournir de
  procédure détaillée pour ces systèmes

On peut partir d'un tutorial récent pour un système Debian/Ubuntu :

https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04-fr

## Mise en place d'une instance dans AWS EC2
## Deploy EC2 virtual machines with Terraform

## Configure your aws local user configuration

You can create a specific AWS user on AWS Web Console with full admin privileges
and use this user (i.e. the corresponding access key and secret access key) later
on. Then you could delete this user at the end of this hands on exercices.

Create your `.aws` personal configuration directory where you will put two files.

`~/.aws/credentials`

```
[default]
aws_access_key_id=YYRR(YOUR ACCESS KEY)UUU
aws_secret_access_key=887AA(YOUR SECRET ACCESS KEY)UUY667
``` 

Make sure only YOU can read that file: `chmod a=rw,go= ~/.aws/credentials`

`~/.aws/config`

```
[default]
region=us-east-2c
output=json
```

We are suggesting us-east-2c as the AMI we will be using is only available there, anyway
it is very likely than any reasonable Debian 9/10 AMI, or a recent CentOS will do (you
may have to adapt Terraform files in this case)

In addition you do have to accept terms and subcribe to the AMI you intend to use
in AWS Web Console (ami-0788cc5a8c3155f0f if you do not change instance.tf).

## Install and run Terraform

Install terraform (last version, or v0.12.18 this procedure has been tested with) and
put terraform binary in a directory which is in your PATH.

You can install a version of Terraform compatible with this deploiement :

```
$ wget https://releases.hashicorp.com/terraform/0.12.19/terraform_0.12.19_linux_amd64.zip
$ unzip terraform_0.12.19_linux_amd64.zip
$ sudo mv terraform /usr/local/bin
```

In addition we need jq which is used by the bastion login script.

Get the files from this repository, build files and initialize Terraform, then construct
your EC2 infrastructure:

```
sudo apt install jq
git clone https://framagit.org/jpython/cours-posgresql-1
cd cours-postgresql-1/
cd ssh-keys/
./generate_keys 
cd ../Scripts/
make
cd ../Tf/
terraform init
terraform plan
terraform apply
```

If every went well you should be able to connect to your bastion ec2 instance:

```
$ ./go_bastion
Bastion public ip: 3.15.139.236
Warning: Permanently added '3.15.139.236' (ECDSA) to the list of known hosts.
Linux ip-10-0-0-42 4.9.0-11-amd64 #1 SMP Debian 4.9.189-3 (2019-09-02) x86_64
...
admin@ip-10-0-0-42:~$ 
```

If it doesn't work you can check your security group in `instances.tf`:

If you have stopped then started your instance its public ip is likely to
have changed, you can ask Terraform to refresh its state:
~~~~
$ terraform refresh
~~~~

```
resource "aws_security_group" "sg_bastion" {
  name = "sg_bastion"
  vpc_id = aws_vpc.vpc_main.id
  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    cidr_blocks = ["YOURPUBLICIP/32"]
...
```

You can fill up with your local network public ip or (it is safe as
ssh is configured to refuse password authentication on our AMI)
`0.0.0.0/0` to allow SSH access from everywhere.

Then re-apply Terraform plan:
```
terraform apply
```

You first have to wait for cloud-init to have finished, at that time a
file `/tmp/cloud-init-bastion-ok` will appear. If you get bored you can
run this waiting for the cloud-init process to disappear :

```
admin@ip-10-0-0-42:~$ watch pstree
```

Then you'll have to wait (again!) for the main server to be properly setted up:

```
admin@ip-10-0-0-42:~$ watch ./check_node_ci 50 
```

When you'll see a file `/tmp/cloud-init-node-ok` you can move forward.

First connect as `admin`to the main server we have deployed:

~~~~
$ ssh 10.0.0.50
$ ssh 10.0.0.50
Linux ip-10-0-0-50 4.9.0-11-amd64 #1 SMP Debian 4.9.189-3 (2019-09-02) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Tue Mar 17 09:15:18 2020 from 10.0.0.42
admin@ip-10-0-0-50:~$ 
~~~~


## Install PostreSQL

On Debian/Ubuntu follow this if PostgreSQL 12 is not available in
stock repositories: 

https://wiki.postgresql.org/wiki/Apt

## From here we go to "Atelier 1" : 

https://framagit.org/jpython/cours-postgresql-2/-/tree/master/atelier1

