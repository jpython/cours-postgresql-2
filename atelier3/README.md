# Contraintes

Les contraintes CHECK sont acceptées en MySQL mais sont ignorées en pratique...

https://www.postgresqltutorial.com/postgresql-check-constraint/

Manuel : https://blog.raveland.org/post/postgresql_sr_fr/ 

# Programmation procédurale côté serveur

Référence : https://www.postgresql.org/docs/12/server-programming.html

Quels langages sont supportés ?

## Procédures stockées

https://www.postgresqltutorial.com/postgresql-create-procedure/

## Fonctions stockéesi


https://www.postgresqltutorial.com/postgresql-create-function/

## Triggers

https://www.postgresqltutorial.com/creating-first-trigger-postgresql/

# Administration serveur

RTFM: https://www.postgresql.org/docs/current/admin.html 

(section 18 et ultérieures)

## Admin : cluster

Manuel : https://www.postgresql.org/docs/12/high-availability.html

https://blog.raveland.org/post/postgresql_sr_fr/

## Admin : optimisation

RAM, RAM, RAM...

Augmenter la mémoire utilisé par PostgreSQL (et la quantié de shmem - mémoire partagée -
disponible sous Linux/UNIX)

Désactiver certaines fonctionnalités de sûreté pendant les importations de données volumineuses.

## Extension : timescaledb

Commercial : https://www.timescale.com/

Présentation : https://severalnines.com/database-blog/introduction-timescaledb

Code : https://github.com/timescale/timescaledb
(attention : mix de licence Apache/non libre)


