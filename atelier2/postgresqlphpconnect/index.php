<?php

require 'vendor/autoload.php';

use PostgreSQLTutorial\Connection as Connection;

try {
    $pdo = Connection::get()->connect();
    echo 'A connection to the PostgreSQL database sever has been established successfully.';
    echo "\n";
} catch (\PDOException $e) {
    echo $e->getMessage();
    echo "\n";
    exit;
}

$stmt = $pdo->query('SELECT * FROM cours');

while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
	echo "{$row['code']}, {$row['titre']}\n";
}

$stmt = $pdo->prepare('SELECT code,titre,nb_inscrits
		      FROM cours JOIN sessions
		      ON cours.id_cours = sessions.id_cours 
	              WHERE cours.code = :code');

$stmt->execute( [ ':code' => 'PGSQL01' ] );

while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
	echo "{$row['code']}, {$row['titre']} {$row['nb_inscrits']}\n";
}
