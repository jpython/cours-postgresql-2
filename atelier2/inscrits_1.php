<?php

if(!isset($_GET['code'])) {
    die('Please provide a code');
}

$code = $_GET['code'];


/**
 * CONNEXION A LA BASE DE DONNEES
 */

const DB_HOST = 'localhost';
const DB_PORT = '5432';
const DB_NAME = 'formations';
const DB_USER = 'admin';
const DB_PASSWORD = 'postgres';

try {
    $pdo = new PDO(sprintf("pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s", DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASSWORD));
} catch(Throwable $e) {
    die("Database connection error: " . $e->getMessage());
}



/**
 * RECUPERATION DU COURS
 */

$stmt = $pdo->prepare("SELECT id, titre FROM cours WHERE code = :code");
$stmt->bindParam(':code', $code);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);

if($result === false) { // Le cours n'a pas été trouvé dans la base
    die('Unknown code');
}

$titreCours = $result['titre'];
$idCours = $result['id'];


/**
 * RECUPERATION DES SESSIONS
 */

$stmt = $pdo->prepare("SELECT date, nb_inscrits, instructeurs.nom || ' ' || instructeurs.prenom AS instructeur FROM sessions INNER JOIN instructeurs ON instructeurs.id = sessions.id_instructeur WHERE sessions.id_cours = :idCours ORDER BY date ASC");
$stmt->bindParam(':idCours', $idCours);
$stmt->execute();

$sessions = [];

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $sessions[] = $row;
}


/**
 * AFFICHAGE DES SESSIONS
 */

?>

<p>Cours : <?= utf8_decode($titreCours) ?> (<?= $code ?>)</p>

<?php
if(empty($sessions)) {
    ?>
    <p>Pas de sessions</p>
    <?php
} else {
    ?>
    <table>
        <tr><th>Date</th><th>Nombre d'inscrits</th><th>Instructeur</th></tr>
        <?php
        foreach($sessions as $session) {
            ?>
            <tr>
                <td><?= (DateTime::createFromFormat('Y-m-d', $session['date']))->format('d/m/Y') ?></td>
                <td><?= $session['nb_inscrits'] ?></td>
                <td><?= $session['instructeur'] ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
}
?>

<style>
    table, tr {
        border: 1px solid #666;
        border-collapse: collapse;
    }

    th, td {
        padding: 5px 10px;
    }
</style>
