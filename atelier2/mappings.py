#!/usr/bin/env python3

import psycopg2

c = psycopg2.connect("")
cur = c.cursor()

from collections import OrderedDict
from datetime import date

dataDict = OrderedDict([
             ('INT', [42]),
             ('DECIMAL(8,2)', [3.14, 42]), 
             ('DATE', ['2020-03-19',date.today()]),
             ('INTEGER[]', [ [1,2,3], (1,2,3) ]),
             ('inventory_item',[ '("fuzzy dice",42,1.99)', ("hello",12,3.14) ]),
             ('mood', [ 'happy' ]),
             ('bytea', [ 'test', b'abcd' ] ),
])

sql = "DROP TYPE IF EXISTS inventory_item"
cur.execute(sql)

sql = """CREATE TYPE inventory_item AS (
    name            text,
    supplier_id     integer,
    price           numeric
);"""

cur.execute(sql)

sql = "DROP TYPE IF EXISTS mood"
cur.execute(sql)

sql = "CREATE TYPE mood AS ENUM ('sad', 'ok', 'happy')"
cur.execute(sql)

c.commit()

for sqltype,values in dataDict.items():
    for value in values:
        sql = "DROP TABLE IF EXISTS test_types"
        cur.execute(sql)
        sql = "CREATE TABLE test_types(test {})".format(sqltype)
        cur.execute(sql)
        try:
            sql = "INSERT INTO test_types(test) VALUES(%s)"
            cur.execute(sql,(value,))
        except psycopg2.errors.DatatypeMismatch as e:
            print('* Erreur sur {} -> {}'.format(type(value).__name__, sqltype))
            print('*',e.args[0])
            # transaction avortée, il faut réinitialiser
            c = psycopg2.connect("")
            cur = c.cursor()
            continue
        sql = "SELECT test FROM test_types;"
        cur.execute(sql)
        data = cur.fetchone()[0]
        print("{} -> {} -> {}".format(type(value),sqltype, type(data)))
        print("{} -> {} -> {}".format(type(value).__name__,sqltype, type(data).__name__))
        print()
        sql = "DROP TABLE IF EXISTS test_types"
        cur.execute(sql)
