variable "token" { 
   default = "JP" # à adapter
}


resource "aws_key_pair" "kp_admin" {
  key_name   = "kp_admin_${var.token}"
  public_key = file("../ssh-keys/id_rsa_aws.pub")
}

resource "aws_key_pair" "kp_admin_int" {
  key_name   = "kp_admin_int_${var.token}"
  public_key = file("../ssh-keys/id_rsa_aws_int.pub")
}


resource "aws_security_group" "sg_bastion" {
  name = "sg_bastion"
  vpc_id = aws_vpc.vpc_main.id
  ingress {
    from_port = "22"
    to_port   = "22"
    protocol  = "tcp"
    #cidr_blocks = ["0.0.0.0/0"]
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "8001"
    to_port   = "8500"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "80"
    to_port   = "80"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = "443"
    to_port   = "443"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "bastion" {
  #ami           = "ami-045fa58af83eb0ff4"
  ami		= "ami-0788cc5a8c3155f0f"
  instance_type = "t2.micro"
  key_name      = "kp_admin_${var.token}"
  vpc_security_group_ids = [aws_security_group.sg_bastion.id,
			    aws_security_group.sg_internal.id]
  subnet_id              = aws_subnet.subnet-a.id
  private_ip 		 = "10.0.0.42"
  associate_public_ip_address = "true"
  user_data = file("../Scripts/bastion_init.sh")
  tags = {
    Name = "bastion"
  }
}

resource "aws_security_group" "sg_internal" {
  name = "sg_internal"
  vpc_id = aws_vpc.vpc_main.id
  ingress {
    from_port = "0"
    to_port   = "0"
    protocol  = "-1"
    cidr_blocks = ["10.0.0.0/24"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "sg_kibana_master" {
  name = "sg_kibana_master"
  vpc_id = aws_vpc.vpc_main.id
  ingress {
    from_port = "5601"
    to_port   = "5601"
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "pg-master" {
  #ami           = "ami-045fa58af83eb0ff4"
  ami		= "ami-0788cc5a8c3155f0f"
  #instance_type = "t2.micro"
  instance_type = "t3.xlarge"
  key_name      = "kp_admin_int_${var.token}"
  vpc_security_group_ids = [aws_security_group.sg_internal.id,
			    aws_security_group.sg_kibana_master.id ]
  subnet_id              = aws_subnet.subnet-a.id
  private_ip 		 = "10.0.0.50"
  associate_public_ip_address = "true"
  user_data = file("../Scripts/node_init.sh")
  tags = {
    Name = "pg-master"
    App  = "pg"
    pg-role = "master"
  }
}

variable "workers_count" {
  default = 0 
}

resource "aws_instance" "workers" {
  #ami           = "ami-045fa58af83eb0ff4"
  count         = var.workers_count
  ami		= "ami-0788cc5a8c3155f0f"
  instance_type = "t3.xlarge"
  key_name      = "kp_admin_int_${var.token}"
  vpc_security_group_ids = [aws_security_group.sg_internal.id]
  subnet_id              = aws_subnet.subnet-a.id
  private_ip 		 = "10.0.0.${count.index + 100}"
  associate_public_ip_address = "true"
  user_data = file("../Scripts/node_init.sh")
  tags = {
    Name = "worker-${count.index + 1}"
    App  = "pg"
    eld-role = "elastic node"
  }
}

output "bastion_ip" {
  value = "${aws_instance.bastion.*.public_ip}"
}

output "master_ip" {
  value = "${aws_instance.pg-master.*.public_ip}"
}


output "bastion_internal_ip" {
  value = "${aws_instance.bastion.*.private_ip}"
}

output "pg_admin_ip" {
  value = "${aws_instance.pg-master.*.private_ip}"
}

