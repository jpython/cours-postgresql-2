#!/usr/bin/env python3

import json
import sys

ignore_error = False

with open('movies_elastic2.json', encoding = 'UTF-8') as infile:
    processed, success = 0,0
    # TODO: factoriser le code pour avoir une boucle plus courte...
    for n,line in enumerate(infile,1):
        if not 'fields' in line:
            continue
        processed += 1
        try:
            line = line.rstrip()
            data = json.loads(line)
        except json.decoder.JSONDecodeError:
            if ignore_error:
                sys.stderr.write('Erreur ligne {} : ignorée\n'.format(n))
                continue
            else:
                pass
            sys.stderr.write('Trying to fix bad JSON at line {}\n'.format(n))
            plot_tag = '"plot" : "'
            plot_start = line.find(plot_tag) + len(plot_tag)
            end_tag = '","'
            plot_end = line.find(end_tag,plot_start)
            #print('***',line)
            #print('***',plot_start,plot_end,line[plot_start:plot_end])
            plot_string = line[plot_start:plot_end]
            line = line[:plot_start] + \
                   plot_string.replace('"',r'\"') + \
                   line[plot_end:]
            #print('***',line)
            data = json.loads(line)
            #print('***',data)
        #traitement des apostrophes dans les champs texte
        #ne serait pas nécessaire si on exécutait directement
        #les requètes préparées en Python
        for key,value in data['fields'].items():
            if isinstance(value,str):
                data['fields'][key] = value.replace("'","''")
            if isinstance(value,list):
                data['fields'][key] = \
                        [ s.replace("'","''") for s in value ]
        fields = json.dumps(data['fields'],ensure_ascii=False)
        text_id = data['id']
        sql = "INSERT INTO movies(text_id,data) " \
              "VALUES ('{}','{}');".format(text_id,fields)
        print(sql)
        success += 1
    sys.stderr.write("Lignes traitées: {}/{}\n".format(success, processed))
         
