CREATE TABLE IF NOT EXISTS employes(
        id INT NOT NULL,
        nom TEXT,
        prenom TEXT,
        departement INT,
        date_de_naissance DATE,
        salaire NUMERIC(8,2)
);

CREATE TABLE IF NOT EXISTS departements(
        id INT NOT NULL,
        nom TEXT,
        manager INT
);
