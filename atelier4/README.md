# Systèmes d'Information Géospatiaux (ou Géographiques) (GIS)

Systèmes d'information stockant et manipulant des données géographiques ou spatiales.
Applications courantes : cartographie, calcul d'itinéraires, extractions de données
statistiques (i.e. total de longueur d'autoroutes partant de Paris, surfaces cultivées
en maïs en Bretagne en 2018), navigation terrestre, maritime et spatiale (l'ISS est
"pilotée" par un logiciel utilisant PostGIS).

Un acteur historique du domaine est la société ESRI (créé en 1969) :

https://fr.wikipedia.org/wiki/Environmental_Systems_Research_Institute

Et, parmi d'autres :

- Open Street Map (projet collaboratif de cartographie)
- Google Map
- Institut Géographique National en France
- PostGIS :-)

## Concepts de base

- Formats sources de données vectorielles :
  * SHP : norme de fait, créé par ESRI
  * Well-known text (WKT)
  * XML Géo
  * GeoJSON 
  * PBF : Open Street Map binary format

cf. https://wiki.openstreetmap.org/wiki/OSM_file_formats


- Concept de projection : sens des coordonnées présentes dans une source
  * 4326 : latitude, longitude
  * 2154 : Lambert93, la norme pour la France
  * Mercator, etc.
  * cf. http://epsg.io/

- Couche dans une carte
  * Données à prendre en compte
  * Peut changer en fonction des besoins 
  * Peut changer en fonction de l'échelle
  * Routes, bâtiments, nature du terrain, etc.

Très bonne introduction aux GIS et à PostGIS : http://rbdd.cnrs.fr/IMG/pdf/postgis20-2039b4.pdf

## Logiciels importants

- outils de conversion de format et projections
- ArcGIS (propriétaire) d'ESRI
- QGIS : client lourd (QT) pour UNIX, Linux et Windows
- Mapbox studio : design de style cartograhiques
- GeoServer et MapServer (JAVA)
- PostGIS : extension GIS de PostgreSQL

Liens: 

https://georezo.net/

http://www.geoinformations.developpement-durable.gouv.fr/spip.php?page=sommaire

https://www.debian.org/blends/gis/index.fr.html

https://www.geoportail.gouv.fr/

https://www.data.gouv.fr/fr/

http://cours-fad-public.ensg.eu/course/view.php?id=84 

https://medium.com/@tjukanov/why-should-you-care-about-postgis-a-gentle-introduction-to-spatial-databases-9eccd26bc42b

# PostGIS

https://postgis.net/install/

https://trac.osgeo.org/postgis/wiki

http://blog.cleverelephant.ca/

## Installation sur Debian/Ubuntu avec le dépot APT de PostgreSQL activé :

~~~~
$ sudo apt install postgis postgresql-12-postgis-3
$ sudo su postgres
postgres@ip-10-0-0-50:/home/admin/cours-posgresql-1/atelier2$ psql
psql (12.2 (Debian 12.2-2.pgdg90+1))
Type "help" for help.

postgres=# CREATE EXTENSION postgis;
CREATE EXTENSION
postgres=# CREATE EXTENSION postgis_topology;
CREATE EXTENSION
postgres=# SELECT PostGIS_version();
            postgis_version
---------------------------------------
 3.0 USE_GEOS=1 USE_PROJ=1 USE_STATS=1
(1 row)
postgres=# \q
postgres@ip-10-0-0-50:/home/admin/cours-posgresql-1/atelier2$ exit
exit
~~~~

## Import de données SHP ou OSM

Télécharge des couches sur la ville de Toulouse :

~~~~
$ wget https://www.geofabrik.de/data/shapefiles_toulouse.zip
$ mkdir toulouse
$ cd toulouse
$ unzip ../shapefiles_toulouse.zip
$ ls *shp
~~~~

On peut examiner les données avec QGIS

![](qgis1.png)

Conversion et import dans PostgreSQL

Création de la base avec PostGIS actif :

à faire en tant que user postgres :

~~~~
createdb template_postgis

# Allows non-superusers the ability to create from this template
psql -d postgres -c "UPDATE pg_database SET datistemplate='true' WHERE datname='template_postgis';"
psql template_postgis -c "create extension postgis"
psql template_postgis -c "create extension postgis_topology"

# Enable users to alter spatial tables.
psql -d template_postgis -c "GRANT ALL ON geometry_columns TO PUBLIC;"
psql -d template_postgis -c "GRANT ALL ON geography_columns TO PUBLIC;"
psql -d template_postgis -c "GRANT ALL ON spatial_ref_sys TO PUBLIC;"
~~~~

à faire en tant qu'utilisateur normal :

~~~~
createdb -T template_postgis toulouse
~~~~

Importation des données

~~~~
$ shp2pgsql -d  gis_osm_landuse_a_07_1.shp > gis_osm_landuse_a_07_1.sql
$ vi gis_osm_landuse_a_07_1.sql
$ psql -d toulouse < gis_osm_landuse_a_07_1.sql
~~~~

On peut automatiser tout ça :

~~~~
for shpf in *shp
do
    shp2pgsql -d $shpf >> output.sql
done
psql -d toulouse < output.sql > /dev/null 2>&1
$ psql -d toulouse
toulouse=# \d gis_osm_routes_07_1
                                        Table "public.gis_osm_routes_07_1"
   Column   |           Type            | Collation | Nullable |                     Default
------------+---------------------------+-----------+----------+--------------------------------------------------
 gid        | integer                   |           | not null | nextval('gis_osm_routes_07_1_gid_seq'::regclass)
 osm_id     | character varying(10)     |           |          |
 lastchange | character varying(20)     |           |          |
 code       | smallint                  |           |          |
 fclass     | character varying(28)     |           |          |
 geomtype   | character varying(1)      |           |          |
 geom       | geometry(MultiLineString) |           |          |
Indexes:
    "gis_osm_routes_07_1_pkey" PRIMARY KEY, btree (gid)
toulouse=# SELECT gid,osm_id,code,fclass,geomtype FROM  gis_osm_routes_07_1 LIMIT 2 \g
 gid | osm_id  | code | fclass | geomtype
-----+---------+------+--------+----------
   1 | 2079954 | 9003 | hiking | R
   2 | 2817688 | 9004 | horse  | R
toulouse=# SELECT gid,osm_id,code,fclass,geomtype,ST_Length(geom) FROM  gis_osm_routes_07_1 LIMIT 2 \g
 gid | osm_id  | code | fclass | geomtype |      st_length
-----+---------+------+--------+----------+----------------------
   1 | 2079954 | 9003 | hiking | R        |  0.09107281593304364
   2 | 2817688 | 9004 | horse  | R        | 0.035149237834068335
toulouse=# SELECT SUM(ST_Length(geom)) FROM gis_osm_routes_07_1;
        sum
-------------------
 4.452508077099528
~~~~

# Génération de cartes

Mapnik: écrit en C++ et disponible comme module Python

Feuille de style : en XML ou MSS (Map Style Sheet, similaire à CSS)

Editeur de feuilles style : Mapbox Studio (node.js)

Compiler et installer mapnik est un un peu coton en ce moment...

# Maptnik 

https://archive.fosdem.org/2018/schedule/event/geo_mapnik/attachments/slides/2574/export/events/attachments/geo_mapnik/slides/2574/2018_FOSDEM_Python_Mapnik_handout.pdf

https://ircama.github.io/osm-carto-tutorials/tile-server-ubuntu/



