#!/usr/bin/env bash

install -d -m 700 /root/.ssh
base64 -d > /root/.ssh/authorized_keys <<EOF
PUBKEY
EOF

apt -y update
apt -y install apt-transport-https

date > /tmp/cloud-init-node-ok

