<?php

try {
	$pgsql = new PDO('pgsql:host=localhost;port=5432;dbname=alexis', 'alexis', 'root');
} catch (Exception $e) {
	die($e->getMessage());
}

if (isset($_GET['code'])) {
	$code = strval($_GET['code']);
	$response = $pgsql->prepare('SELECT cours.code, cours.titre, sessions.nb_inscrits, sessions.date 
		FROM sessions 
		INNER JOIN cours ON sessions.id_cours = cours.id_cours 
		WHERE cours.code = ?;');
	$response->execute([$code]);
	$data = $response->fetchAll();

	foreach ($data as $record) { ?>
		<div style="border: 1px black solid; margin-bottom: 20px;">
			<h1>Session du <?= $record['date'] ?></h1>
			<h2><?= utf8_decode($record['titre']) ?></h2>
			<p>code : <?= $record['code'] ?></p>
			<p>inscrits : <?= $record['nb_inscrits'] ?></p>
		</div>
	<?php }
}

?>
