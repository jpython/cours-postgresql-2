DROP TABLE IF EXISTS employes;
CREATE TABLE IF NOT EXISTS employes(
        id SERIAL PRIMARY KEY,
        nom TEXT,
        prenom TEXT,
        departement INT,
        date_de_naissance DATE,
        salaire NUMERIC(8,2)
);

DROP TABLE IF EXISTS departements;
CREATE TABLE IF NOT EXISTS departements(
        id SERIAL,
        nom TEXT,
        manager INT,
	PRIMARY KEY(id)
);
