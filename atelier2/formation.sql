DROP INDEX IF EXISTS sessions_id_instructeur_idx CASCADE;
DROP INDEX IF EXISTS sessions_id_cours_idx CASCADE;
DROP INDEX IF EXISTS enseigne_id_instructeur_idx CASCADE;
DROP INDEX IF EXISTS enseigne_id_cours_idx CASCADE;

DROP TABLE IF EXISTS cours CASCADE;
CREATE TABLE cours(
	id_cours SERIAL PRIMARY KEY,
	code VARCHAR(10) UNIQUE,
	titre TEXT,
	duree INT
);

DROP TABLE IF EXISTS instructeurs CASCADE;
CREATE TABLE instructeurs(
	id_instructeur SERIAL PRIMARY KEY,
	nom TEXT,
	prenom TEXT
);

DROP TABLE IF EXISTS sessions CASCADE;
CREATE TABLE sessions(
	id_session SERIAL PRIMARY KEY,
	id_cours INT REFERENCES cours(id_cours),
	date DATE,
	nb_inscrits INT,
	id_instructeur INT REFERENCES instructeurs(id_instructeur)
);

DROP TABLE IF EXISTS enseigne CASCADE;
CREATE TABLE enseigne(
	id_instructeur INT REFERENCES instructeurs(id_instructeur),
	id_cours INT REFERENCES cours(id_cours)
);

CREATE INDEX sessions_id_instructeur_idx ON sessions(id_instructeur);
CREATE INDEX sessions_id_cours_idx ON sessions(id_cours);
CREATE INDEX enseigne_id_instructeur_idx ON enseigne(id_instructeur);
CREATE INDEX enseigne_id_cours_idx ON enseigne(id_cours);
