const { Pool, Client } = require('pg')

const pool = new Pool({
    user: "postgres",
    host: "localhost",
    database: "postgres",
    password: "postgres",
    port: 5432
})

pool.query("select cours.code, COALESCE(SUM(sessions.nb_inscrits),0) AS nb_inscrits, COUNT(sessions.*) AS nb_sessions from cours LEFT JOIN sessions ON sessions.id_cours = cours.id_cours GROUP BY cours.code;", (err, res) => {
    console.log(err, res)
    pool.end()
})