CREATE TABLE movies(id serial primary key, text_id varchar(20), data jsonb);

select * from (select id, text_id, data->'title' AS title,data->'actors' ? 'Scarlett Johansson' AS scarlett_is_here from movies) AS q1 WHERE scarlett_is_here;
