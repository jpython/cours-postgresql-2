# Importation de JSON dans PostgreSQL

PG propose deux types pour stocker du json : json et jsonb. Le 
second est plus efficace et est indexable de façon spécifique.

Des requêtes SQL peuvent accéder efficatement aux champs et éléments
du champs de types jsonb. On peut en extraire des valeurs pour les
insérer dans la même ou une autre table.

1. Créer une base movies
2. Créer une table movies: id serial primary key, text_id VARCHAR(20), data jsonb
3. Exécuter le script d'import dans psql -d movies

~~~~
createdb movies

CREATE TABLE movies(id SERIAL PRIMARY KEY, text_id VARCHAR(20), data JSONB;
\i import_movies.sql
~~~~

## Restructurer en SQL les données JSON

1. Ajouter un champ title TEXT à la table
2. Mettre à jour la table (UPDATE ...) pour renseigner ce champ avec le titre
   extrait du champ data (JSON)
3. Faire la même chose pour la date de sortie

Exemple qui montre bien que la gestion des dates est loin d'être simple,
voir cet article : https://phili.pe/posts/timestamps-and-time-zones-in-postgresql/

Ici on veut juste récupérer le _jour_ de sortie du film, pas un instant 
absolu dans le temps (cette information n'est pas dans le json...), et
le json fournit un instant absolu (suffize 'Z') mais toujours à 00h00:00.

Ceci empêche PG de convertir directement la release\_date en TIMESTAMP
(instant absolu) ou TIMESTAMPTZ (instant avec time zone), on y arrive
cependant en convertissant d'abord en TEXT puis
en DATE pour ne garder que le jour : `(data->'release_date')::TEXT::DATE`

~~~~
ALTER TABLE movies ADD COLUMN title TEXT, ADD COLUMN release_date DATE;

UPDATE movies SET title = data->'title';

UPDATE movies SET release_date = (data->'release_date')::TEXT::DATE;
~~~~

## Extraction dans une autre table

1. Créer les tables actors et directors (id et nom) avec UNIQUE comme contrainte sur le nom
2. Insérer les données issues du champs data de movies dans ces tables
3. Concevoir les relations plays\_in et directs entre movies et ces tables
5. Insérer les données de ces relations 

~~~~
CREATE TABLE actors(id SERIAL PRIMARY KEY, name TEXT UNIQUE);
CREATE TABLE realisators(id SERIAL PRIMARY KEY, name TEXT UNIQUE);

INSERT INTO actors(name) SELECT DISTINCT jsonb_array_elements(data->'actors') AS actor FROM movies;
INSERT INTO directors(name) SELECT DISTINCT jsonb_array_elements(data->'directors') AS director FROM movies;

CREATE TABLE plays_in(movie INT REFERENCES movies(id),actor INT REFERENCES actors(id) ) ;
CREATE TABLE directs(movie INT REFERENCES movies(id),director INT REFERENCES directors(id) ) ;
~~~~

Insérer les données des tables de relations implique de comparer les champs noms des acteurs et
réalisateur pour insérer uniquement leur id dans les tables. Coûteux, il est difficile d'éviter
de construire le produit cartésien des films et des acteurs... Voir si on peut optimiser:

~~~~
INSERT INTO plays_in(movie,actor) SELECT movies.id,actors.id AS actor FROM movies,actors
      WHERE actors.name IN (SELECT jsonb_array_elements(movies.data->'actors')::TEXT AS actor
                              FROM movies AS m WHERE m.id = movies.id) ;
~~~~

Bien vérifier qu'on a bien insérer des données, et pas trop...
~
~~~~
movies=# SELECT * FROM plays_in WHERE movie = 42;
 movie | actor
-------+-------
    42 |  1427
    42 |  4162
    42 |  5338
(3 rows)

movies=# SELECT title,actors.name FROM movies JOIN plays_in ON movies.id = plays_in.movie JOIN actors ON plays_in.actor = actors.id WHERE movies.id = 42;
    title    |       name
-------------+------------------
 "Furious 6" | "Dwayne Johnson"
 "Furious 6" | "Paul Walker"
 "Furious 6" | "Vin Diesel"
(3 rows)
~~~~

On peut tester en inversant les rôles de movie et actor dans la sous-requètes.

On peut réfléchir à trouver une solution sans aucun produit cartésient.

Requète pour insérer les réalisateurs :

~~~~
INSERT INTO directs(movie,director)
SELECT movies.id,directors.id
  FROM movies,directors WHERE directors.name IN
(SELECT jsonb_array_elements(movies.data->'directors')::TEXT
   FROM movies as m WHERE m.id = movies.id) ; 

SELECT title,directors.name FROM movies JOIN directs ON movies.id = directs.movie JOIN directors ON directs.director = directors.id WHERE movies.id = 42;
    title    |     name
-------------+--------------
 "Furious 6" | "Justin Lin"
(1 row)

SELECT movies.id,title,COUNT(*) AS nb_directors FROM movies JOIN directs ON movies.id = directs.movie JOIN directors ON directs.director = directors.id GROUP BY movies.id HAVING count(*) > 2;
  id  |                title                 | nb_directors
------+--------------------------------------+--------------
 1595 | "Peter Pan"                          |            3
  799 | "Gone with the Wind"                 |            3
   90 | "V/H/S/2"                            |            3
...
 4275 | "The Great Mouse Detective"          |            3
 3803 | "The Rescuers"                       |            3
  397 | "V/H/S"                              |            3
 2500 | "The Fox and the Hound"              |            3
(55 rows)
~~~~

Requètes pour ajouter et renseigner un champs rating :

~~~~
ALTER TABLE movies ADD COLUMN rating DECIMAL(3,2);
UPDATE movies SET rating = (data->'rating')::DECIMAL(3,2);;
~~~~

Quels sont les acteurs ayant joué dans un film réalisé par Denis Villeneuve ?

~~~~
SELECT actors.name,movies.title FROM actors JOIN plays_in ON actors.id = plays_in.actor JOIN directs ON plays_in.movie = directs.movie JOIN directors ON directs.director = directors.id JOIN movies ON directs.movie = movies.id WHERE directors.name ~ 'Villeneuve';
~~~~

Utilisez EXPLAIN/ANALYZE sur une requète faisant une jointure sur plays_in, ajoutez ensuite des index sur les deux champs de cette requète, comparer le "query plan" obtenu ensuite pour la même requète. 

~~~~
EXPLAIN ANALYZE SELECT title,actors.name FROM movies JOIN plays_in ON movies.id = plays_in.movie JOIN actors ON plays_in.actor = actors.id WHERE movies.id = 42;
                                                           QUERY PLAN
---------------------------------------------------------------------------------------------------------------------------------
 Nested Loop  (cost=0.56..284.70 rows=3 width=33) (actual time=0.268..0.915 rows=3 loops=1)
   ->  Nested Loop  (cost=0.28..259.79 rows=3 width=21) (actual time=0.262..0.904 rows=3 loops=1)
         ->  Index Scan using movies_pkey on movies  (cost=0.28..8.30 rows=1 width=21) (actual time=0.017..0.018 rows=1 loops=1)
               Index Cond: (id = 42)
         ->  Seq Scan on plays_in  (cost=0.00..251.46 rows=3 width=8) (actual time=0.243..0.883 rows=3 loops=1)
               Filter: (movie = 42)
               Rows Removed by Filter: 14834
   ->  Index Scan using actors_pkey on actors  (cost=0.28..8.30 rows=1 width=20) (actual time=0.003..0.003 rows=1 loops=3)
         Index Cond: (id = plays_in.actor)
 Planning Time: 0.195 ms
 Execution Time: 0.939 ms
(11 rows)

CREATE INDEX play_in_movie_idx ON plays_in(movie);
CREATE INDEX play_in_actor_idx ON plays_in(actor);

EXPLAIN ANALYZE SELECT title,actors.name FROM movies JOIN plays_in ON movies.id = plays_in.movie JOIN actors ON plays_in.actor = actors.id WHERE movies.id = 42;
                                                              QUERY PLAN
--------------------------------------------------------------------------------------------------------------------------------------
 Nested Loop  (cost=4.87..47.66 rows=3 width=33) (actual time=0.039..0.048 rows=3 loops=1)
   ->  Nested Loop  (cost=4.59..22.76 rows=3 width=21) (actual time=0.034..0.038 rows=3 loops=1)
         ->  Index Scan using movies_pkey on movies  (cost=0.28..8.30 rows=1 width=21) (actual time=0.006..0.007 rows=1 loops=1)
               Index Cond: (id = 42)
         ->  Bitmap Heap Scan on plays_in  (cost=4.31..14.43 rows=3 width=8) (actual time=0.024..0.027 rows=3 loops=1)
               Recheck Cond: (movie = 42)
               Heap Blocks: exact=3
               ->  Bitmap Index Scan on play_in_movie_idx  (cost=0.00..4.31 rows=3 width=0) (actual time=0.020..0.020 rows=3 loops=1)
                     Index Cond: (movie = 42)
   ->  Index Scan using actors_pkey on actors  (cost=0.28..8.30 rows=1 width=20) (actual time=0.003..0.003 rows=1 loops=3)
         Index Cond: (id = plays_in.actor)
 Planning Time: 0.394 ms
 Execution Time: 0.073 ms
(13 rows)
~~~~
